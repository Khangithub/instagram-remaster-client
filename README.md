# Write your query or mutation here
{
  posts {
    id
    caption
    commentList {
      text
      createdAt
      displayName
      uid
    }
    createdAt
    likeCount
    displayName
    peopleLikedPostList {
      displayName
      uid
    }
    uid
    uploadedMediaList {
      mediaUrl
      type
    }
    avatar
  }
}
