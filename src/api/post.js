import graphqlRequest from './constant';

export const getPost = async ({id}) => {
  const query = `
      query postQuery($id: ID!) {
        post(id: $id) {
          id
          caption
          commentList {
            text
            createdAt
            displayName
            uid
          }
          createdAt
          likeCount
          displayName
          peopleLikedPostList {
            displayName
            uid
          }
          uid
          uploadedMediaList {
            mediaUrl
            type
          }
          avatar
        }
      }  
    `;

  const {post} = await graphqlRequest(query, {id});
  return post;
};
