const endpointURL =
  'http://localhost:5000/facebook-clone-2a127/asia-east2/api/graphql';

async function graphqlRequest(query, variables = {}) {
  const response = await fetch(endpointURL, {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify({query, variables}),
  });
  const responseBody = await response.json();
  if (responseBody.errors) {
    const message = responseBody.errors
      .map((error) => error.message)
      .join('\n');

    throw new Error(message);
  }
  return responseBody.data;
}

export default graphqlRequest;
