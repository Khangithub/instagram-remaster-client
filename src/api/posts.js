import graphqlRequest from './constant';

export const getPosts = async () => {
  const query = `{
        posts {
          id
          caption
          commentList {
            text
            createdAt
            displayName
            uid
          }
          createdAt
          likeCount
          displayName
          peopleLikedPostList {
            displayName
            uid
          }
          uid
          uploadedMediaList {
            mediaUrl
            type
          }
          avatar
        }
      }
      `;

  const {posts} = await graphqlRequest(query);

  return posts;
};

export const createPost = async ({
  caption,
  inputUploadedMediaList,
  displayName,
  avatar,
  uid,
}) => {
  const mutation = `
  mutation CreatePost($input: CreatePostInput){
    post: createPost(
      input: $input
    ){
      id
      caption
      likeCount
    }
  }
  `;

  const input = {
    caption,
    displayName,
    avatar,
    uid,
    inputUploadedMediaList,
  };

  const {post} = await graphqlRequest(mutation, {
    input,
  });

  return post;
};

export const likePost = async ({id, displayName, avatar, uid}) => {
  console.log(id, displayName, avatar, uid, 'api like post');
  const mutation = `
  mutation LikePost($input: LikePostInput) {
    post: likePost(input: $input) {
      id
      caption
      peopleLikedPostList {
        uid
        displayName
      }
    }
  }`;

  const input = {id, uid, displayName, avatar};
  const {post} = await graphqlRequest(mutation, {
    input,
  });

  return post;
};

export const unlikePost = async ({id, index}) => {
  const mutation = `
  mutation UnlikePost($input: UnlikePostInput) {
    post: unlikePost(input: $input) {
      id
      caption
      peopleLikedPostList {
        uid
        displayName
      }
    }
  }
  `;

  const input = {index, id};
  const {post} = await graphqlRequest(mutation, {
    input,
  });

  return post;
};
