import React from 'react';
import {IconButton} from '@material-ui/core';
function CustomedIconBtn({src, alt, onClick}) {
  return (
    <IconButton onClick={onClick}>
      <img
        src={src}
        style={{
          height: '25px',
          width: '25px',
          cursor: 'pointer'
        }}
        alt={alt}
      />
    </IconButton>
  );
}

export default CustomedIconBtn;
