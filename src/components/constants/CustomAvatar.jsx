import React from 'react';
import {IconButton} from '@material-ui/core';

function CustomAvatar({src, alt, avatarOnly}) {
  return avatarOnly ? (
    <img
      src={src}
      style={{
        height: '30px',
        width: '30px',
        borderRadius: '50%',
      }}
      draggable={false}
      alt={alt}
    />
  ) : (
    <IconButton>
      <img
        src={src}
        style={{
          height: '30px',
          width: '30px',
          borderRadius: '50%',
          border: '1px solid lightgray',
        }}
        draggable={false}
        alt={alt}
      />
    </IconButton>
  );
}

export default CustomAvatar;
