import {Switch, Route} from 'react-router-dom';
import Home from '../pages/Home';
import Post from '../pages/Post';
import Login from '../pages/Login';
import Story from '../pages/Story';
import ProtectedRoute from '../utils/ProtectedRoute';

function App() {
  return (
    <Switch>
      <ProtectedRoute exact path="/" component={Home} />
      <ProtectedRoute exact path="/post/:postId" component={Post} />
      <ProtectedRoute exact path="/story/:storyIndex" component={Story} />
      <Route exact from="/login" render={(props) => <Login {...props} />} />
    </Switch>
  );
}

export default App;
