import React, {useState} from 'react';
import SearchIcon from '@material-ui/icons/Search';
import CancelIcon from '@material-ui/icons/Cancel';
import {useHistory} from 'react-router-dom';
import {
  SendIcon,
  HomeIcon,
  HeartIcon,
  CompassIcon,
  LogoIcon,
} from '../../assets/imgs';
import {ClickAwayListener} from '@material-ui/core';
import {useSelector} from 'react-redux';

import {Row, Col} from 'react-bootstrap';
import './Header.css';
import CustomedIconBtn from '../constants/CustomedIconBtn';
import CustomedAvatar from '../constants/CustomAvatar';

function Header() {
  const [visibleInput, setVisibleInput] = useState(false);
  const history = useHistory();
  const user = useSelector((state) => state.user.user);

  return (
    <div className="container-fluid">
      <Row className="header">
        <Col
          xs={12}
          sm={4}
          className="header__logo"
          onClick={() => history.push('/')}
        >
          <img className="app__headerImage" src={LogoIcon} alt="" />
        </Col>
        <Col xs={12} sm={4} className="header__input__container">
          {visibleInput && (
            <ClickAwayListener onClickAway={() => setVisibleInput(false)}>
              <div>
                <SearchIcon />
                <input type="text" placeholder="Tim kiem" autoFocus />
                <CancelIcon onClick={() => setVisibleInput(!visibleInput)} />
              </div>
            </ClickAwayListener>
          )}

          {!visibleInput && (
            <button onClick={() => setVisibleInput(!visibleInput)}>
              <SearchIcon />
              <span>Tim kiem</span>
            </button>
          )}
        </Col>
        <Col xs={12} sm={4} className="header__option__buttons">
          <CustomedIconBtn src={HomeIcon} alt="HomeIcon" />
          <CustomedIconBtn src={SendIcon} alt="SendIcon" />
          <CustomedIconBtn src={CompassIcon} alt="CompassIcon" />
          <CustomedIconBtn src={HeartIcon} alt="HeartIcon" />

          {user && <CustomedAvatar src={user.url} alt="avatar" />}
        </Col>
      </Row>
    </div>
  );
}

export default Header;
