import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import {Carousel} from 'react-responsive-carousel';
import './Post.scss';
import CustomedAvatar from '../../components/constants/CustomAvatar';
import CustomedIconBtn from '../../components/constants/CustomedIconBtn';
import {
  HeartIcon,
  RedHeartIcon,
  MessageIcon,
  SendIcon,
} from '../../assets/imgs';
import {useDispatch, useSelector} from 'react-redux';
import {likePostRequest, unlikePostRequest} from '../../actions/posts';

function Post({
  data: {
    id,
    avatar,
    uid,
    displayName,
    uploadedMediaList,
    peopleLikedPostList,
    likeCount,
  },
  index
}) {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);
  const [postReact, setPostReact] = useState(false);

  return (
    <div className="post">
      <div className="post__header__container">
        <div onClick={() => history.push(`/post/${id}`)}>
          <CustomedAvatar src={avatar} alt={displayName} avatarOnly />
          <span>{displayName}</span>
        </div>

        <div>...</div>
      </div>

      <div className="post__media__container">
        <div>
          <Carousel>
            {uploadedMediaList.map(({mediaUrl, type}, index) => (
              <div key={index}>
                {type.includes('image') && (
                  <div
                    className="media"
                    style={{backgroundImage: `url("${mediaUrl}")`}}
                  ></div>
                )}
                {type.includes('video') && (
                  <video className="media" src={mediaUrl} controls alt="" />
                )}
              </div>
            ))}
          </Carousel>
        </div>
        <div
          style={{
            display: postReact ? 'flex' : 'none',
            justifyContent: 'center',
          }}
        >
          <img
            src={RedHeartIcon}
            alt=""
            style={{
              height: '100px',
              width: '100px',
              position: 'relative',
              top: '-300px',
              marginBottom: '-100px',
            }}
          />
        </div>
        <div>
          <CustomedIconBtn
            src={
              peopleLikedPostList.map((each) => each.uid).includes(user.uid)
                ? RedHeartIcon
                : HeartIcon
            }
            alt="HeartIcon"
            onClick={async () => {
              await dispatch(
                peopleLikedPostList.map((each) => each.uid).includes(user.uid)
                  ? unlikePostRequest({id, index})
                  : likePostRequest({
                      id,
                      uid: user.uid,
                      displayName: user.displayName,
                      avatar: user.avatar,
                    })
              );
              await setPostReact(true);
              await setTimeout(() => setPostReact(false), 1500);
            }}
          />
          <CustomedIconBtn src={MessageIcon} alt="MessageIcon" />
          <CustomedIconBtn src={SendIcon} alt="SendIcon" />
        </div>
      </div>

      <div className="post__comment__list__container">
        <div>
          {peopleLikedPostList.length > 0
            ? `${peopleLikedPostList
                .map((each) => each.displayName)
                .join()} và ${likeCount} người khác đã thích`
            : `${likeCount} lượt thích`}
        </div>
      </div>

      <div className="post__add__comment__container"></div>
    </div>
  );
}

export default Post;
