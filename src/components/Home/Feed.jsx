import React, {useState} from 'react';
import './Feed.css';
import {IconButton} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import StoryThumbnailList from './StoryThumbnailList';
import PostList from './PostList';
import CreatePostModal from './CreatePostModal';

function Feed() {
  const [modalShow, setModalShow] = useState(false);

  return (
    <div className="feed">
      <StoryThumbnailList />

      <div className="feed__post__list__container">
        <PostList />
      </div>

      <CreatePostModal
        className="feed__create__post__modal"
        show={modalShow}
        onHide={() => setModalShow(false)}
      />

      <IconButton onClick={() => setModalShow(!modalShow)}>
        {modalShow ? (
          <CloseIcon fontSize="large" />
        ) : (
          <AddIcon fontSize="large" />
        )}
      </IconButton>
    </div>
  );
}

export default Feed;
