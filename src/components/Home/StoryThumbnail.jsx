import React from 'react';
import './StoryThumbnail.css';
import {useHistory} from 'react-router-dom';

export default function StoryThumbnailList({avatar, username, storyIndex}) {
  const history = useHistory();

  return (
    <div
      className="story__thumbnail"
      onClick={() => history.push(`/story/${storyIndex}`)}
    >
      <div className="story__avatar__container">
        <img src={avatar} alt="avatar" draggable={false} />
      </div>
      <p>{username}</p>
    </div>
  );
}
