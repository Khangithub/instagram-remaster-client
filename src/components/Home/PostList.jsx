import React, {useEffect} from 'react';
import './PostList.css';
import {connect} from 'react-redux';
import {getPostsRequest} from '../../actions/posts';
import Post from './Post';

function PostList({posts, getPostsRequest}) {
  useEffect(() => {
    getPostsRequest();
  }, [getPostsRequest]);


  return (
    <div className="post__list">
      {posts.posts.map((data, index) => (
        <Post key={data.id} data={data} index={index} />
      ))}
      
    </div>
  );
}

export default connect(({posts}) => ({posts}), {
  getPostsRequest,
})(PostList);
