import React, {useState} from 'react';
import {createPostRequest} from '../../actions/posts';
import Picker from 'emoji-picker-react';
import {Modal} from 'react-bootstrap';
import CancelIcon from '@material-ui/icons/Cancel';
import GroupIcon from '@material-ui/icons/Group';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import PhotoIcon from '@material-ui/icons/Photo';
import VerticalSplitIcon from '@material-ui/icons/VerticalSplit';
import HorizontalSplitIcon from '@material-ui/icons/HorizontalSplit';
import EmojiEmotionsOutlinedIcon from '@material-ui/icons/EmojiEmotionsOutlined';
import Loading from '../Loading/Loading';
import {storage} from '../../firebase';
import {IconButton, Avatar} from '@material-ui/core';
import './CreatePostModal.css';
import {useSelector, useDispatch} from 'react-redux';

export default function CreatePostModal(props) {
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();
  const [mediaList, setMediaList] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [caption, setCaption] = useState('');
  const [viewStyle, setViewStyle] = useState(false);
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);

  const onFileChange = (e) => {
    for (let i = 0; i < e.target.files.length; i++) {
      const mediaFile = e.target.files[i];
      mediaFile['id'] = Math.random();
      var previewMedia = URL.createObjectURL(mediaFile);

      setMediaList((preVal) => [
        ...preVal,
        {mediaFile, previewMedia, type: mediaFile.type},
      ]);
    }
  };

  const handleCreatePost = async (e) => {
    e.preventDefault();
    setUploading(true);

    const imgFileList = mediaList.map(({mediaFile}) => mediaFile);

    const promises = imgFileList.map((file) => {
      let ref = storage.ref(`instagram/${file.name}`);
      return ref.put(file).then(() => ref.getDownloadURL());
    });

    const uploadedMediaList = await Promise.all(promises);

    await dispatch(
      createPostRequest({
        caption,
        inputUploadedMediaList: uploadedMediaList.map((media, index) => {
          return {inputMediaUrl: media, inputType: mediaList[index].type};
        }),
        displayName: user.displayName,
        avatar: user.url,
        uid: user.uid,
      })
    );
    await setMediaList([]);
    await setUploading(false);
    await setCaption('');
    await setViewStyle(false);
    await setShowEmojiPicker(false);
    await props.onHide();
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <div
        style={{
          position: 'relative',
          overflowY: viewStyle ? 'scroll' : 'hidden',
          overflowX: 'hidden',
          height: '500px',
        }}
      >
        <div className="create__post__modal">
          <div className="create__post__modal__header">
            <div></div>
            <h5>Tạo bài viết</h5>
            <IconButton onClick={() => props.onHide()}>
              <CancelIcon fontSize="large" />
            </IconButton>
          </div>

          <div className="create__post__modal__avatar">
            <Avatar src={user.url} />

            <div>
              <p>{user.displayName}</p>
              <div>
                <GroupIcon fontSize="small" />
                <span>&nbsp; Bạn bè &nbsp;</span>
                <ArrowDropDownIcon fontSize="small" />
              </div>
            </div>
          </div>
          {uploading ? (
            <Loading />
          ) : (
            <>
              <textarea
                cols="30"
                rows="6"
                placeholder={`${user.displayName} ơi, bạn đang nghĩ gì thế ?`}
                onChange={(e) => setCaption(e.target.value)}
                value={caption}
              />
              <div className="create__post__modal_preview__style__btn__container">
                <IconButton onClick={() => setViewStyle(!viewStyle)}>
                  {viewStyle ? (
                    <VerticalSplitIcon fontSize="small" />
                  ) : (
                    <HorizontalSplitIcon fontSize="small" />
                  )}
                </IconButton>
                <IconButton
                  onClick={() => setShowEmojiPicker(!showEmojiPicker)}
                >
                  <EmojiEmotionsOutlinedIcon fontSize="large" />
                </IconButton>
                {showEmojiPicker && (
                  <Picker
                    onEmojiClick={(event, emojiObject) => {
                      setCaption((preVal) => preVal + emojiObject.emoji);
                    }}
                  />
                )}
              </div>
              <div className="create__post__modal__preview__img__list">
                {mediaList.map(({previewMedia, type}, index) => (
                  <div
                    className="create__post__modal_preview__media__container"
                    key={index}
                  >
                    <IconButton
                      onClick={() =>
                        setMediaList(mediaList.filter((__, i) => i !== index))
                      }
                    >
                      <CancelIcon fontSize="large" />
                    </IconButton>

                    {type.includes('image') && (
                      <img src={previewMedia} alt="" />
                    )}
                    {type.includes('video') && (
                      <video src={previewMedia} controls autoPlay alt="" />
                    )}
                  </div>
                ))}
              </div>

              <div className="create__post__modal__add__img__btn">
                <span>Thêm vào bài viết</span>

                <form enctype="multipart/form-data">
                  <input
                    multiple
                    id="icon-button-file"
                    type="file"
                    accept="video/*,image/*"
                    onChange={onFileChange}
                  />

                  <label htmlFor="icon-button-file">
                    <IconButton
                      color="primary"
                      aria-label="upload picture"
                      component="span"
                    >
                      <PhotoIcon fontSize="large" />
                    </IconButton>
                  </label>
                </form>
              </div>
            </>
          )}

          <button
            disabled={uploading ? true : false}
            style={{backgroundColor: uploading ? 'lightgrey' : '#1877f2'}}
            onClick={handleCreatePost}
          >
            Đăng
          </button>
        </div>
      </div>
    </Modal>
  );
}
