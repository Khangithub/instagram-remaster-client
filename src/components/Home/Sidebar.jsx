import React from 'react';
import {useHistory} from 'react-router-dom';
import './Sidebar.css';
import {useSelector} from 'react-redux';
import {sugestFriendList} from '../../utils/mock-data';
import {saveState} from '../../utils/localStorage';
import CustomAvatar from '../constants/CustomAvatar';
import {useDispatch} from 'react-redux';
import {setUserSuccess} from '../../actions/user';

function Sidebar() {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);

  return (
    <div className="sidebar">
      <div className="sidebar__nav__account__container">
        <img src={user.url} alt="" />

        <div>
          <p>{user.displayName}</p>
          <p>{user.displayName}</p>
        </div>

        <span
          onClick={async () => {
            await saveState('user', undefined);
            await dispatch(
              setUserSuccess({
                user: undefined,
                error: {},
              })
            );
            history.push('/login');
          }}
        >
          Chuyển
        </span>
      </div>

      <div className="side__suggest__friend__list__container">
        <div>
          <span>Gợi ý cho bạn</span>
          <span>Xem tất cả</span>
        </div>

        {sugestFriendList.map((friend, index) => (
          <div className="sidebar__suggest__friend" key={index}>
            <CustomAvatar src={friend.url} alt={friend.username} avatarOnly />

            <div>
              <p>{friend.username}</p>
              <p>{friend.reason}</p>
            </div>

            <span>Theo dõi</span>
          </div>
        ))}
      </div>

      <div className="sidebar__footer">
        <div>
          <span> Giới thiệu</span>
          <span>Trợ giúp</span>
          <span> Báo chí</span>
          <span> API</span>
          <span> Việc làm</span>
          <span> Quyền riêng tư</span>
          <span>Điều khoản</span>
          <span> Vị trí</span>
          <span> Tài khoản liên quan nhất</span>
          <span>Hashtag</span>
          <span>Ngôn ngữ</span>
        </div>

        <div>
          <span>© 2020 INSTAGRAM FROM FACEBOOK</span>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
