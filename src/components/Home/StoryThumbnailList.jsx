import React from 'react';
import StoryThumbnail from './StoryThumbnail';
import './StoryThumbnailList.css';
import {Fade} from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import {stories} from '../../utils/mock-data';

const STORY_CAPACITY = 7;

export default function StoryThumbnailList() {

  const renderStories = (stories) => {
    var storyContainerList = [];
    var storyContainer = [];

    for (var i = 0; i < stories.length; i++) {
      if (i % STORY_CAPACITY !== 0 || i === 0) {
        storyContainer.push(stories[i]);
      }

      if (i % STORY_CAPACITY === 6) {
        storyContainerList.push(storyContainer);
        storyContainer = [];
        storyContainer.push(stories[i]);
      }
    }

    return storyContainerList.map((stories, containerIndex) => (
      <div className="each-fade" key={containerIndex}>
        {stories.map(({username, avatar}, eachIndex) => (
          <StoryThumbnail
            username={username}
            avatar={avatar}
            key={eachIndex}
            storyIndex={STORY_CAPACITY * containerIndex + eachIndex}
          />
        ))}
      </div>
    ));
  };

  return (
    <div className="story__thumbnail__list">
      <Fade>{renderStories(stories)}</Fade>
    </div>
  );
}
