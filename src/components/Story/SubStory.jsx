import React from 'react';
import './SubStory.css';
import {useHistory} from 'react-router-dom';

export default function SubStory({
  subStory,
  subStoryIndex,
  setVidStoryOtps,
  imgNextStory,
  setUserNavigateStory,
}) {
  const history = useHistory();
  const {created, username, avatar, thumbnailURL} = subStory;

  return (
    <div
      className="sub__story"
      onClick={() => {
        setUserNavigateStory(true);
        setTimeout(() => {
          setUserNavigateStory(false);
        }, 300);
        setVidStoryOtps({
          ...setVidStoryOtps,
          playVid: true,
          enableVidSound: true,
        });
        clearTimeout(imgNextStory);
        history.push(`/story/${subStoryIndex}`);
      }}
    >
      <div
        className="sub__story__bg"
        style={{backgroundImage: `url("${thumbnailURL}")`}}
      ></div>

      <div className="sub__story__user__info">
        <img src={avatar} alt="" draggable={false} />
        <p>{username}</p>
        <p>{created} giờ</p>
      </div>
    </div>
  );
}
