import React, {useEffect} from 'react';
import {stories} from '../../utils/mock-data';
import {useSelector, useDispatch} from 'react-redux';
import './CurrentStory.css';
import {IconButton} from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import SendIcon from '@material-ui/icons/Send';
import PauseIcon from '@material-ui/icons/Pause';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import {nextStory} from '../../actions/story';

export default function CurrentStory({
  userNavigateStory,
  vidStoryOpts,
  setVidStoryOtps,
  setImgNextStory,
}) {
  const dispatch = useDispatch();
  const story = useSelector((state) => state.story);
  const {created, username, avatar, mediaList} = stories[story.mainStoryIndex];

  useEffect(() => {
    const progressList = Array.from(document.querySelectorAll('.progress'));

    for (var i = 0; i < progressList.length; i++) {
      if (i === story.subStoryIndex) {
        progressList[i].classList.add('progress__loading__start');
      } else {
        progressList[i].classList.remove('progress__loading__start');
      }
    }

    const initProgress = progressList[story.subStoryIndex];

    initProgress.addEventListener(
      'animationend',
      function () {
        initProgress.classList.remove('progress__loading__start');
      },
      false
    );
  }, [story.subStoryIndex]);

  return (
    <div
      className={`current__story ${userNavigateStory && 'fadeNScale'}
`}
    >
      <div className="current__story__header">
        <div className="current__story__progress__container">
          {mediaList.map((media, index) => (
            <div
              key={index}
              style={{
                animationDuration: `${media.duration}s`,
                animationPlayState: `${
                  !vidStoryOpts.playVid ? 'paused' : 'running'
                }`,
              }}
              className="progress"
            ></div>
          ))}
        </div>
        <div className="current__story__controls">
          <div className="current__story__controls__user__info">
            <img src={avatar} alt="" />

            <span>{username}</span>

            <span>{created} giờ</span>
          </div>
          <div className="current__story__option__btn__container">
            {mediaList[story.subStoryIndex].type === 'video' && (
              <>
                <IconButton
                  onClick={() => {
                    setVidStoryOtps({
                      ...vidStoryOpts,
                      playVid: !vidStoryOpts.playVid,
                    });
                    const vid = document.getElementById('vid');
                    if (vid !== null) {
                      if (vidStoryOpts.playVid) {
                        vid.pause();
                      } else {
                        vid.play();
                      }
                    } else {
                      console.log('this element is not video');
                      return;
                    }
                  }}
                >
                  {vidStoryOpts.playVid ? (
                    <PauseIcon fontSize="small" />
                  ) : (
                    <PlayArrowIcon fontSize="small" />
                  )}
                </IconButton>
                <IconButton
                  onClick={() => {
                    setVidStoryOtps({
                      ...vidStoryOpts,
                      enableVidSound: !vidStoryOpts.enableVidSound,
                    });
                    const vid = document.getElementById('vid');
                    if (vid !== null) {
                      if (vidStoryOpts.enableVidSound) {
                        vid.muted = true;
                      } else {
                        vid.muted = false;
                      }
                    } else {
                      console.log('this element is not video');
                      return;
                    }
                  }}
                >
                  {vidStoryOpts.enableVidSound ? (
                    <VolumeUpIcon fontSize="small" />
                  ) : (
                    <VolumeOffIcon fontSize="small" />
                  )}
                </IconButton>
              </>
            )}

            <IconButton>
              <MoreHorizIcon fontSize="small" />
            </IconButton>
          </div>
        </div>
      </div>
      <div className="current__story__body">
        {mediaList[story.subStoryIndex].type === 'image' ? (
          <img
            id="img"
            src={mediaList[story.subStoryIndex].url}
            alt=""
            onLoad={() => {
              var image = document.getElementById('img');
              var isLoaded = image.complete && image.naturalHeight !== 0;
              if (isLoaded) {
                setImgNextStory(
                  setTimeout(() => {
                    dispatch(nextStory());
                  }, mediaList[story.subStoryIndex].duration * 1000)
                );
              } else {
                console.log('can not load the image');
              }
            }}
          />
        ) : (
          <video
            src={mediaList[story.subStoryIndex].url}
            autoPlay
            id="vid"
            onEnded={() => dispatch(nextStory())}
          />
        )}
      </div>
      <div className="current__story__footer">
        <input type="text" placeholder={`Trả lời ${username} ... `} />
        <IconButton>
          <SendIcon />
        </IconButton>
      </div>
    </div>
  );
}
