import {useEffect} from 'react';
import {connect} from 'react-redux';
import {getPostsRequest} from '../actions/posts';
import Header from '../components/Header/Header';
import Sidebar from '../components/Home/Sidebar';
import Feed from '../components/Home/Feed';

import './Home.css';

function Home({posts, getPostsRequest}) {
  useEffect(() => {
    getPostsRequest();
  }, [getPostsRequest]);

  if (Object.keys(posts.error).length > 0) {
    return <div>{JSON.stringify(posts.error)}</div>;
  }

  return (
    <div className="home">
      <Header />
      <div className="home__container">
        <Feed />
        <Sidebar />
      </div>
    </div>
  );
}

export default connect(({posts}) => ({posts}), {
  getPostsRequest,
})(Home);
