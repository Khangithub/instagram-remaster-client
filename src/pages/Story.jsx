import React, {useState, useEffect} from 'react';
import {IconButton} from '@material-ui/core';
import {InstagramIcon, CloseIcon} from '../assets/imgs';

import SubStory from '../components/Story/SubStory';
import CurrentStory from '../components/Story/CurrentStory';

import {useParams, useHistory} from 'react-router-dom';
import {stories} from '../utils/mock-data';
import {useSelector, useDispatch} from 'react-redux';
import {setStory, nextStory, preStory} from '../actions/story';
import {Col} from 'react-bootstrap';

import './Story.css';

function Story() {
  const dispatch = useDispatch();
  const {storyIndex} = useParams();
  const history = useHistory();
  const story = useSelector((state) => state.story);
  const [vidStoryOpts, setVidStoryOtps] = useState({
    playVid: true,
    enableVidSound: true,
  });
  const [imgNextStory, setImgNextStory] = useState(null);
  const [userNavigateStory, setUserNavigateStory] = useState(false);

  useEffect(() => {
    dispatch(setStory(parseInt(storyIndex)));
  }, [dispatch, storyIndex]);

  return (
    <div className="stories">
      <Col sm={3} className="stories__prev__stories__container">
        <IconButton className="stories__btn" onClick={() => history.push('/')}>
          <img src={InstagramIcon} alt="" />
        </IconButton>

        <div className="stories__container">
          {story.mainStoryIndex - 2 >= 0 && (
            <SubStory
              setUserNavigateStory={setUserNavigateStory}
              vidStoryOpts={vidStoryOpts}
              setVidStoryOtps={setVidStoryOtps}
              imgNextStory={imgNextStory}
              setImgNextStory={setImgNextStory}
              subStory={stories[story.mainStoryIndex - 2]}
              subStoryIndex={story.mainStoryIndex - 2}
            />
          )}

          {story.mainStoryIndex - 1 >= 0 && (
            <SubStory
              setUserNavigateStory={setUserNavigateStory}
              vidStoryOpts={vidStoryOpts}
              setVidStoryOtps={setVidStoryOtps}
              imgNextStory={imgNextStory}
              setImgNextStory={setImgNextStory}
              subStory={stories[story.mainStoryIndex - 1]}
              subStoryIndex={story.mainStoryIndex - 1}
            />
          )}

          {!(story.mainStoryIndex === 0 && story.subStoryIndex === 0) && (
            <button
              onClick={() => {
                setUserNavigateStory(true);
                setTimeout(() => {
                  setUserNavigateStory(false);
                }, 300);

                setVidStoryOtps({
                  ...setVidStoryOtps,
                  playVid: true,
                  enableVidSound: true,
                });
                clearTimeout(imgNextStory);
                dispatch(preStory());
              }}
            >
              ◀
            </button>
          )}
        </div>
      </Col>
      <Col sm={6} className="current__story__container">
        <CurrentStory
          userNavigateStory={userNavigateStory}
          vidStoryOpts={vidStoryOpts}
          setVidStoryOtps={setVidStoryOtps}
          imgNextStory={imgNextStory}
          setImgNextStory={setImgNextStory}
        />
      </Col>

      <Col sm={3} className="stories__next__stories__container">
        <IconButton className="stories__btn" onClick={() => {}}>
          <img src={CloseIcon} alt="" />
        </IconButton>

        <div className="stories__container">
          {story.mainStoryIndex < stories.length - 1 && (
            <button
              onClick={() => {
                setUserNavigateStory(true);
                setTimeout(() => {
                  setUserNavigateStory(false);
                }, 300);

                setVidStoryOtps({
                  ...setVidStoryOtps,
                  playVid: true,
                  enableVidSound: true,
                });
                clearTimeout(imgNextStory);
                dispatch(nextStory());
              }}
            >
              ▶
            </button>
          )}
          {story.mainStoryIndex + 1 <= stories.length - 1 && (
            <SubStory
              setUserNavigateStory={setUserNavigateStory}
              vidStoryOpts={vidStoryOpts}
              setVidStoryOtps={setVidStoryOtps}
              imgNextStory={imgNextStory}
              setImgNextStory={setImgNextStory}
              subStory={stories[story.mainStoryIndex + 1]}
              subStoryIndex={story.mainStoryIndex + 1}
            />
          )}

          {story.mainStoryIndex + 2 <= stories.length - 1 && (
            <SubStory
              setUserNavigateStory={setUserNavigateStory}
              vidStoryOpts={vidStoryOpts}
              setVidStoryOtps={setVidStoryOtps}
              imgNextStory={imgNextStory}
              setImgNextStory={setImgNextStory}
              subStory={stories[story.mainStoryIndex + 2]}
              subStoryIndex={story.mainStoryIndex + 2}
            />
          )}
        </div>
      </Col>
    </div>
  );
}

export default Story;
