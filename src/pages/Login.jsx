import React from 'react';
import './Login.css';
import {
  LoginLogin,
  FacebookIcon,
  AndroidInstagramAppIcon,
  AppleInstagramAppIcon,
} from '../assets/imgs';
import {useHistory, useLocation} from 'react-router-dom';
import {auth, provider} from '../firebase';
import {saveState} from '../utils/localStorage';
import {useDispatch} from 'react-redux';
import {setUserSuccess, setUserFailed} from '../actions/user';

function Login() {
  const history = useHistory();
  const {state} = useLocation();
  const dispatch = useDispatch();

  const signIn = async () => {
    try {
      const result = await auth.signInWithPopup(provider);
      const {url} = result.additionalUserInfo.profile.picture.data;
      const {uid, displayName} = result.user;

      await saveState('user', {
        url,
        uid,
        displayName,
      });

      await dispatch(
        setUserSuccess({
          user: {
            url,
            uid,
            displayName,
          },
        })
      );

      history.push(state ? state.from.pathname : '/');
    } catch (error) {
      dispatch(
        setUserFailed({
          error,
        })
      );
    }
  };

  return (
    <div className="login">
      <div className="login__form__container">
        <img src={LoginLogin} alt="" />
        <div className="login__button" onClick={() => signIn()}>
          <img src={FacebookIcon} alt="" />
          <span>Đăng nhập bằng facebook</span>
        </div>
        <p>Quên mật khẩu ?</p>
      </div>

      <div className="login__download__app__container">
        <p>Tải ứng dụng</p>
        <div>
          <img src={AppleInstagramAppIcon} alt="" />
          <img src={AndroidInstagramAppIcon} alt="" />
        </div>
      </div>

      <div className="login__footer">
        <div>
          <span>Giới thiệu</span>
          <span>Blog</span>
          <span> Việc làm</span>
          <span> Trợ giúp</span>
          <span> API</span>
          <span>Quyền riêng tư</span>
          <span>Điều khoản</span>
          <span> Tài khoản liên quan nhất</span>
          <span> Hashtag</span>
          <span> Vị trí</span>
        </div>

        <div>
          <span>Tiếng Việt 🔽 </span>
          <span>© 2020 Instagram from Facebook</span>
        </div>
      </div>
    </div>
  );
}

export default Login;
