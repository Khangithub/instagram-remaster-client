import React, {useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {connect} from 'react-redux';
import {getPostRequest} from '../actions/post';

function Post({post, getPostRequest}) {
  const {postId} = useParams();

  useEffect(() => {
    getPostRequest(postId);
  }, [getPostRequest, postId]);

  if (Object.keys(post.error).length > 0) {
    return <div>{JSON.stringify(post.error)}</div>;
  }
  
  return <div>{JSON.stringify(post)}</div>;
}

export default connect(({post}) => ({post}), {
  getPostRequest,
})(Post);
