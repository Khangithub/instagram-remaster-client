import {Types} from '../actions/post';

const INTIAL_STATE = {
  post: {},
  postId: '',
  error: {},
};

export default function post(state = INTIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_POST_REQUEST: {
      return {
        ...state,
        postId: action.postId,
      };
    }
    case Types.GET_POST_SUCCESS: {
      return {
        ...state,
        post: action.payload.post,
      };
    }
    case Types.GET_POST_FAILED: {
      return {
        ...state,
        error: action.payload.error,
      };
    }
    default: {
      return state;
    }
  }
}
