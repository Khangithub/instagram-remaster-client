import {Types} from '../actions/story';
import {stories} from '../utils/mock-data';

const INTIAL_STATE = {
  mainStoryIndex: 0,
  subStoryIndex: 0,
};

export default function story(state = INTIAL_STATE, action) {
  switch (action.type) {
    case Types.SET_STORY:
      return {
        ...state,
        mainStoryIndex: action.payload.mainStoryIndex,
        subStoryIndex: 0,
      };
    case Types.NEXT_STORY:
      if (
        state.subStoryIndex >=
        stories[state.mainStoryIndex].mediaList.length - 1
      ) {
        return {
          ...state,
          mainStoryIndex:
            state.mainStoryIndex + 1 > stories.length - 1
              ? stories.length - 1
              : state.mainStoryIndex + 1,
          subStoryIndex: 0,
        };
      } else {
        return {
          ...state,
          subStoryIndex: state.subStoryIndex + 1,
        };
      }

    case Types.PRE_STORY:
      if (state.subStoryIndex <= 0) {
        return {
          ...state,
          mainStoryIndex:
            state.mainStoryIndex - 1 < 0 ? 0 : state.mainStoryIndex - 1,
          subStoryIndex:
            state.mainStoryIndex - 1 < 0
              ? 0
              : stories[state.mainStoryIndex - 1].mediaList.length - 1,
        };
      } else {
        return {
          ...state,
          subStoryIndex: state.subStoryIndex - 1,
        };
      }

    default:
      return state;
  }
}
