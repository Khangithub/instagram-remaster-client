import {Types} from '../actions/user';
import { loadState } from '../utils/localStorage';

const INTIAL_STATE = {
  user: loadState('user'),
  error: {},
};

export default function user(state = INTIAL_STATE, action) {
  switch (action.type) {
    case Types.SET_USER_SUCCESS: {
      return {
        ...state,
        user: action.payload.user,
      };
    }

    case Types.SET_USER_FAILED: {
      return {
        ...state,
        error: action.payload.error,
      };
    }

    default: {
      return state;
    }
  }
}
