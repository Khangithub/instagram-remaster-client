import {combineReducers} from 'redux';
import PostsReducer from './posts';
import PostReducer from './post';
import UserReducer from './user';
import StoryReducer from './story';

export default combineReducers({
  posts: PostsReducer,
  post: PostReducer,
  user: UserReducer,
  story: StoryReducer,
});
