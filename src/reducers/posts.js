import {Types} from '../actions/posts';

const INTIAL_STATE = {
  posts: [],
  error: {},
};

export default function posts(state = INTIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_POSTS_SUCCESS: {
      return {
        ...state,
        posts: action.payload.posts,
      };
    }
    case Types.GET_POSTS_FAILED: {
      return {
        ...state,
        error: action.payload.error
      }
    }
    default: {
      return state;
    }
  }
}
