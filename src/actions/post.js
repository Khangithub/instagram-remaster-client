export const Types = {
  GET_POST_REQUEST: 'post/get_post_request',
  GET_POST_SUCCESS: 'post/get_post_success',
  GET_POST_FAILED: 'post/get_post_failed',
};

export const getPostRequest = (postId) => ({
  type: Types.GET_POST_REQUEST,
  payload: {
    postId,
  },
});

export const getPostSuccess = ({post}) => ({
  type: Types.GET_POST_SUCCESS,
  payload: {
    post,
  },
});

export const getPostFailed = ({error}) => ({
  type: Types.GET_POST_FAILED,
  payload: {
    error,
  },
});


