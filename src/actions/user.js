export const Types = {
  SET_USER_SUCCESS: 'user/set_user_success',
  SET_USER_FAILED: 'user/set_user_failed',
};

export const setUserSuccess = ({user}) => ({
  type: Types.SET_USER_SUCCESS,
  payload: {
    user,
  },
});

export const setUserFailed = ({error}) => ({
  type: Types.SET_USER_FAILED,
  payload: {
    error,
  },
});
