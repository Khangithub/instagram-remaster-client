export const Types = {
  GET_POSTS_REQUEST: 'posts/get_posts_request',
  GET_POSTS_SUCCESS: 'posts/get_posts_success',
  GET_POSTS_FAILED: 'posts/get_posts_failed',
  CREATE_POST_REQUEST: 'posts/create_post_request',
  DELETE_POST_REQUEST: 'posts/delete_post_request',
  LIKE_POST_REQUEST: 'post/like_post_request',
  UNLIKE_POST_REQUEST: 'post/unlike_post_request',
};

export const createPostRequest = ({
  caption,
  inputUploadedMediaList,
  displayName,
  avatar,
  uid,
}) => ({
  type: Types.CREATE_POST_REQUEST,
  payload: {
    caption,
    inputUploadedMediaList,
    displayName,
    avatar,
    uid,
  },
});

export const getPostsRequest = () => ({
  type: Types.GET_POSTS_REQUEST,
});

export const getPostsSuccess = ({posts}) => ({
  type: Types.GET_POSTS_SUCCESS,
  payload: {
    posts,
  },
});

export const getPostsFailed = ({error}) => ({
  type: Types.GET_POSTS_FAILED,
  payload: {
    error,
  },
});

export const likePostRequest = ({id, uid, displayName, avatar}) => ({
  type: Types.LIKE_POST_REQUEST,
  payload: {
    id,
    uid,
    displayName,
    avatar,
  },
});

export const unlikePostRequest = ({id, index}) => ({
  type: Types.UNLIKE_POST_REQUEST,
  payload: {
    id,
    index
  },
});
