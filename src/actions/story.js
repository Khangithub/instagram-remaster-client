export const Types = {
  NEXT_STORY: 'story/next_story',
  PRE_STORY: 'story/pre_story',
  SET_STORY: 'story/set_story',
};

export const nextStory = () => ({
  type: Types.NEXT_STORY,
});

export const preStory = () => ({
  type: Types.PRE_STORY,
});

export const setStory = (mainStoryIndex) => ({
  type: Types.SET_STORY,
  payload: {
    mainStoryIndex,
  },
});
