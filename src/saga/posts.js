import {takeEvery, takeLatest, call, fork, put} from 'redux-saga/effects';
import * as actions from '../actions/posts';
import * as api from '../api/posts';

function* getPosts() {
  try {
    const posts = yield call(api.getPosts);
    yield put(
      actions.getPostsSuccess({
        posts,
      })
    );
  } catch (error) {
    error.location = 'sai ở chỗ lấy toàn bộ danh sách post';
    yield put(
      actions.getPostsFailed({
        error,
      })
    );
  }
}

function* watchGetPostsRequest() {
  yield takeEvery(actions.Types.GET_POSTS_REQUEST, getPosts);
}

function* createPost({
  payload: {caption, inputUploadedMediaList, displayName, avatar, uid},
}) {
  try {
    yield call(api.createPost, {
      caption,
      inputUploadedMediaList,
      displayName,
      avatar,
      uid,
    });
    yield call(getPosts);
  } catch (error) {
    error.location = 'sai ở chỗ tạo post mới';
    yield put(
      actions.getPostsFailed({
        error,
      })
    );
  }
}

function* watchCreateNewPostRequest() {
  yield takeLatest(actions.Types.CREATE_POST_REQUEST, createPost);
}

function* likePost({payload: {uid, id, displayName, avatar}}) {
  try {
    yield call(api.likePost, {
      id,
      displayName,
      avatar,
      uid,
    });
    yield call(getPosts);
  } catch (error) {
    error.location = 'sai ở likePost';
    yield put(
      actions.getPostsFailed({
        error,
      })
    );
  }
}

function* watchLikePost() {
  yield takeLatest(actions.Types.LIKE_POST_REQUEST, likePost);
}

function* unlikePost({payload: {id, index}}) {
  try {
    yield call(api.unlikePost, {
      id,
      index,
    });
    yield call(getPosts);
  } catch (error) {
    error.location = 'sai ở unlikePost';
    yield put(
      actions.getPostsFailed({
        error,
      })
    );
  }
}

function* watchUnlikePost() {
  yield takeLatest(actions.Types.UNLIKE_POST_REQUEST, unlikePost);
}

const postsSaga = [
  fork(watchGetPostsRequest),
  fork(watchCreateNewPostRequest),
  fork(watchLikePost),
  fork(watchUnlikePost),
];

export default postsSaga;
