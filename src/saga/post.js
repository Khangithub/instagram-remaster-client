import {takeEvery, call, fork, put} from 'redux-saga/effects';
import * as actions from '../actions/post';
import * as api from '../api/post';

function* getPost({payload: {postId}}) {
  try {
    const post = yield call(api.getPost, {id: postId});
    yield put(
      actions.getPostSuccess({
        post,
      })
    );
  } catch (error) {
    error.location = 'sai ở chỗ lấy ra một post cụ thể';
    yield put(
      actions.getPostFailed({
        error,
      })
    );
  }
}

function* watchGetPostRequest() {
  yield takeEvery(actions.Types.GET_POST_REQUEST, getPost);
}

const postSaga = [fork(watchGetPostRequest)];

export default postSaga;
