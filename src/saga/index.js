import PostsSaga from './posts';
import PostSaga from './post';
import {all} from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([...PostsSaga, ...PostSaga]);
}
