import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyDQ9eG1YtXU_HzreKJNvT9Aghvnqt0p5ok',
  authDomain: 'facebook-clone-2a127.firebaseapp.com',
  databaseURL: 'https://facebook-clone-2a127.firebaseio.com',
  projectId: 'facebook-clone-2a127',
  storageBucket: 'facebook-clone-2a127.appspot.com',
  messagingSenderId: '496131693678',
  appId: '1:496131693678:web:fce8bfd129403eaece5a7f',
});

const db = firebaseApp.firestore();
const auth = firebaseApp.auth();
const storage = firebaseApp.storage();
const provider = new firebase.auth.FacebookAuthProvider();

export {db, auth, storage, provider};
